<?php
declare(strict_types=1);

namespace App\Domain\Video;

interface VideoRepository
{
    /**
     * @return Video[]
     */
    public function findAll(): array;

    /**
     * @param string $id
     * @return Video
     * @throws VideoNotFoundException
     */
    public function findVideoOfId(string $id): Video;
}
