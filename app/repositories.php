<?php
declare(strict_types=1);

use App\Domain\User\UserRepository;
use App\Infrastructure\Persistence\User\InMemoryUserRepository;
use App\Domain\User2\User2Repository;
use App\Infrastructure\Persistence\User2\InMemoryUser2Repository;
use App\Domain\Video\VideoRepository;
use App\Infrastructure\Persistence\Video\InMemoryVideoRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(InMemoryUserRepository::class),
        User2Repository::class => \DI\autowire(InMemoryUser2Repository::class),
        VideoRepository::class => \DI\autowire(InMemoryVideoRepository::class),
    ]);
};
